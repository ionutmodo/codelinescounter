﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace CodeLinesCounter
{
    public partial class CodeLinesCounter : Form
    {
        MyMap map = new MyMap();
        List<string> extensions = new List<string>();

        public CodeLinesCounter()
        {
            InitializeComponent();
            foreach(Control c in extensionsGroupBox.Controls)
            {
                if(c is CheckBox)
                {
                    ((CheckBox)c).CheckedChanged += checkBox_CheckedChanged;
                }
            }
        }

        private void openButton_Click(object sender, System.EventArgs e)
        {
            FolderBrowserDialog browser = new FolderBrowserDialog();
            if(browser.ShowDialog() == DialogResult.OK)
            {
                folderTextBox.Text = browser.SelectedPath;
            }
        }

        private void Scrape(string path)
        {
            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path);
                foreach (string file in files)
                {
                    FileInfo info = new FileInfo(file);
                    if(extensions.Contains(info.Extension))
                    {
                        map.Add(info.Extension, file);
                    }
                }
                string[] directories = Directory.GetDirectories(path);
                foreach (string directory in directories)
                {
                    Scrape(directory);
                }
            }
        }

        private void countButton_Click(object sender, System.EventArgs e)
        {
            countButton.Enabled = false;
            if (!Directory.Exists(folderTextBox.Text))
            {
                MessageBox.Show("The directory path doesn't exist!");
            }
            else
            {
                map.Clear();
                extensions.Clear();
                foreach (Control c in extensionsGroupBox.Controls)
                {
                    if ((c is CheckBox) && ((CheckBox)c).Checked)
                    {
                        extensions.Add(c.Text);
                    }
                }
                Scrape(folderTextBox.Text);
                CountAndLog();
            }
            countButton.Enabled = true;
        }

        private void CountAndLog()
        {
            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
            string[] files = map.GetValues();
            int total = 0;
            foreach (string file in files)
            {
                if (File.Exists(file))
                {
                    FileInfo info = new FileInfo(file);
                    int lines = CountLines(file);
                    total = total + lines;
                    list.Add(new KeyValuePair<string, int>(info.Name, lines));
                }
            }
            ShowNewForm(list);
        }

        private int CountLines(string file)
        {
            int count = 0;
            StreamReader reader = new StreamReader(file);
            string line = null;
            while((line = reader.ReadLine()) != null)
            {
                ++count;
            }
            reader.Close();
            return count;
        }

        private void ShowNewForm(List<KeyValuePair<string, int>> list)
        {
            ResultsForm form = new ResultsForm(list);
            form.ShowDialog();
        }
        
        private void selectButton_Click(object sender, System.EventArgs e)
        {
            SetCheck(true);
            selectButton.Enabled = false;
            deselectButton.Enabled = true;
        }

        private void deselectButton_Click(object sender, System.EventArgs e)
        {
            SetCheck(false);
            selectButton.Enabled = true;
            deselectButton.Enabled = false;
            countButton.Enabled = false;
        }

        private void SetCheck(bool value)
        {
            foreach (Control c in extensionsGroupBox.Controls)
            {
                if (c is CheckBox)
                {
                    ((CheckBox)c).Checked = value;
                }
            }
        }

        private void checkBox_CheckedChanged(object sender, System.EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            countButton.Enabled = cb.Checked || LeastOneChecked();
        }

        private bool LeastOneChecked()
        {
            bool status = false;
            int size = extensionsGroupBox.Controls.Count;
            for (int i = 0; i < size && !status; ++i)
            {
                Control c = extensionsGroupBox.Controls[i];
                if (c is CheckBox)
                {
                    CheckBox cb = c as CheckBox;
                    if (cb.Checked)
                    {
                        status = true;
                    }
                }
            }
            return status;
        }
    }
}
