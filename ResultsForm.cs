﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeLinesCounter
{
    public partial class ResultsForm : Form
    {
        public ResultsForm(List<KeyValuePair<string, int>> list)
        {
            InitializeComponent();

            int total = 0;
            foreach(KeyValuePair<string, int> pair in list)
            {
                total += pair.Value;
                string[] row = { pair.Key, pair.Value.ToString() };
                grid.Rows.Add(row);
            }
            resultsLabel.Text = "Total lines count: " + total;
        }
    }
}
