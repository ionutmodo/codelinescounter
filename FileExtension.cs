﻿using System;

namespace CodeLinesCounter
{
    enum FileExtension
    {
        asm,
        c,
        cpp,
        cs,
        h,
        html,
        java,
        json,
        php,
        py,
        rb,
        txt,
        xml
    }
}
