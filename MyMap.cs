﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CodeLinesCounter
{
    public class MyMap
    {
        List<string> keys;
        List<List<string>> values;

        public MyMap()
        {
            keys = new List<string>();
            values = new List<List<string>>();
        }

        public void Add(string key, string value)
        {
            if(!keys.Contains(key))
            {
                keys.Add(key);
                values.Add(new List<string>());
            }
            int index = keys.IndexOf(key);
            values[index].Add(value);
        }

        public void Clear()
        {
            keys.Clear();
            foreach(List<string> list in values)
            {
                list.Clear();
            }
            values.Clear();
        }

        public string[] GetValues()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < keys.Count; ++i)
            {
                for (int j = 0; j < values[i].Count; ++j)
                {
                    list.Add(values[i][j]);
                }
            }
            return list.ToArray();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < keys.Count; ++i)
            {
                builder.Append(keys[i] + " :");
                for(int j = 0; j < values[i].Count; ++j)
                {
                    builder.Append(values[i][j] + " ");
                }
                builder.Append("\n");
            }
            return builder.ToString();
        }
    }
}