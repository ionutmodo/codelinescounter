﻿namespace CodeLinesCounter
{
    partial class CodeLinesCounter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CodeLinesCounter));
            this.folderTextBox = new System.Windows.Forms.TextBox();
            this.openButton = new System.Windows.Forms.Button();
            this.extensionsGroupBox = new System.Windows.Forms.GroupBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.selectButton = new System.Windows.Forms.Button();
            this.deselectButton = new System.Windows.Forms.Button();
            this.countButton = new System.Windows.Forms.Button();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.extensionsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // folderTextBox
            // 
            this.folderTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.folderTextBox.Location = new System.Drawing.Point(11, 12);
            this.folderTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.folderTextBox.Name = "folderTextBox";
            this.folderTextBox.Size = new System.Drawing.Size(404, 21);
            this.folderTextBox.TabIndex = 0;
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(422, 12);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(50, 23);
            this.openButton.TabIndex = 1;
            this.openButton.Text = ". . .";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // extensionsGroupBox
            // 
            this.extensionsGroupBox.Controls.Add(this.checkBox16);
            this.extensionsGroupBox.Controls.Add(this.checkBox15);
            this.extensionsGroupBox.Controls.Add(this.checkBox14);
            this.extensionsGroupBox.Controls.Add(this.selectButton);
            this.extensionsGroupBox.Controls.Add(this.deselectButton);
            this.extensionsGroupBox.Controls.Add(this.countButton);
            this.extensionsGroupBox.Controls.Add(this.checkBox13);
            this.extensionsGroupBox.Controls.Add(this.checkBox12);
            this.extensionsGroupBox.Controls.Add(this.checkBox11);
            this.extensionsGroupBox.Controls.Add(this.checkBox10);
            this.extensionsGroupBox.Controls.Add(this.checkBox9);
            this.extensionsGroupBox.Controls.Add(this.checkBox8);
            this.extensionsGroupBox.Controls.Add(this.checkBox7);
            this.extensionsGroupBox.Controls.Add(this.checkBox6);
            this.extensionsGroupBox.Controls.Add(this.checkBox5);
            this.extensionsGroupBox.Controls.Add(this.checkBox4);
            this.extensionsGroupBox.Controls.Add(this.checkBox3);
            this.extensionsGroupBox.Controls.Add(this.checkBox2);
            this.extensionsGroupBox.Controls.Add(this.checkBox1);
            this.extensionsGroupBox.Location = new System.Drawing.Point(12, 42);
            this.extensionsGroupBox.Name = "extensionsGroupBox";
            this.extensionsGroupBox.Size = new System.Drawing.Size(460, 207);
            this.extensionsGroupBox.TabIndex = 2;
            this.extensionsGroupBox.TabStop = false;
            this.extensionsGroupBox.Text = "Select extensions";
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(6, 126);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(56, 20);
            this.checkBox15.TabIndex = 10;
            this.checkBox15.Text = ".css";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(76, 74);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(45, 20);
            this.checkBox14.TabIndex = 15;
            this.checkBox14.Text = ".js";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // selectButton
            // 
            this.selectButton.Location = new System.Drawing.Point(346, 114);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(108, 25);
            this.selectButton.TabIndex = 3;
            this.selectButton.Text = "Select all";
            this.selectButton.UseVisualStyleBackColor = true;
            this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // deselectButton
            // 
            this.deselectButton.Enabled = false;
            this.deselectButton.Location = new System.Drawing.Point(346, 145);
            this.deselectButton.Name = "deselectButton";
            this.deselectButton.Size = new System.Drawing.Size(108, 25);
            this.deselectButton.TabIndex = 4;
            this.deselectButton.Text = "Deselect all";
            this.deselectButton.UseVisualStyleBackColor = true;
            this.deselectButton.Click += new System.EventHandler(this.deselectButton_Click);
            // 
            // countButton
            // 
            this.countButton.Enabled = false;
            this.countButton.Location = new System.Drawing.Point(346, 176);
            this.countButton.Name = "countButton";
            this.countButton.Size = new System.Drawing.Size(108, 25);
            this.countButton.TabIndex = 5;
            this.countButton.Text = "Count lines";
            this.countButton.UseVisualStyleBackColor = true;
            this.countButton.Click += new System.EventHandler(this.countButton_Click);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(76, 178);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(53, 20);
            this.checkBox13.TabIndex = 19;
            this.checkBox13.Text = ".txt";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(6, 22);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(63, 20);
            this.checkBox12.TabIndex = 6;
            this.checkBox12.Text = ".asm";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(76, 48);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(63, 20);
            this.checkBox11.TabIndex = 14;
            this.checkBox11.Text = ".json";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(145, 22);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(59, 20);
            this.checkBox10.TabIndex = 20;
            this.checkBox10.Text = ".xml";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(76, 152);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(47, 20);
            this.checkBox9.TabIndex = 18;
            this.checkBox9.Text = ".rb";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(76, 126);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(50, 20);
            this.checkBox8.TabIndex = 17;
            this.checkBox8.Text = ".py";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(6, 152);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(41, 20);
            this.checkBox7.TabIndex = 11;
            this.checkBox7.Text = ".h";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(6, 178);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(65, 20);
            this.checkBox6.TabIndex = 12;
            this.checkBox6.Text = ".html";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(76, 100);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(59, 20);
            this.checkBox5.TabIndex = 16;
            this.checkBox5.Text = ".php";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(75, 22);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(64, 20);
            this.checkBox4.TabIndex = 13;
            this.checkBox4.Text = ".java";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(6, 100);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(48, 20);
            this.checkBox3.TabIndex = 9;
            this.checkBox3.Text = ".cs";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(6, 74);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(58, 20);
            this.checkBox2.TabIndex = 8;
            this.checkBox2.Text = ".cpp";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 48);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(40, 20);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.Text = ".c";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(145, 48);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(59, 20);
            this.checkBox16.TabIndex = 21;
            this.checkBox16.Text = ".hpp";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // CodeLinesCounter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 261);
            this.Controls.Add(this.extensionsGroupBox);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.folderTextBox);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "CodeLinesCounter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Code Lines Counter";
            this.extensionsGroupBox.ResumeLayout(false);
            this.extensionsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox folderTextBox;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.GroupBox extensionsGroupBox;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.Button countButton;
        private System.Windows.Forms.Button selectButton;
        private System.Windows.Forms.Button deselectButton;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox16;
    }
}

